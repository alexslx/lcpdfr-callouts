﻿namespace TaticalEnforcer.Callouts
{
    using System.Linq;

    using GTA;

    using LCPD_First_Response.Engine;
    using LCPD_First_Response.LCPDFR.API;
    using LCPD_First_Response.LCPDFR.Callouts;
    using System.Windows.Forms;
    using LCPD_First_Response.Engine.Timers;
    using System.Collections.Generic;

    /// <summary>
    /// Gruppe 6 requires assistance from LCPD in order to transport a large amount of money.
    /// </summary>
    [CalloutInfo("BankTransport", ECalloutProbability.Always)]
    internal class BankTransport : Callout
    {
        /// <summary>
        /// Callout state machine definition.
        /// </summary>
        private enum ECalloutState
        {
            STATE_INIT,
            STATE_WAITING_DRIVER,
            STATE_WAITING_PLAYER,
            STATE_IN_TRANSIT,
            STATE_ARRIVED
        }

        /// <summary>
        /// Security state of the vehicle based on player location.
        /// </summary>
        private enum EVehicleSecurity
        {
            SECURITY_NONE,
            SECURITY_ESCORTED
        }

        /// <summary>
        /// Collection of starting points for this callout.
        /// </summary>
        private SpawnPoint[] startPositions = new SpawnPoint[]
		{
			new SpawnPoint(180.21f, new Vector3(-59.64f, 1120.59f, 14.32f)),
			new SpawnPoint(180.21f, new Vector3(-59.64f, 1120.59f, 14.32f))
		};

        /// <summary>
        /// Collection of destionations for this callout.
        /// </summary>
        private Vector3[] finishPositions = new Vector3[]
		{
			new Vector3(-59.07f, -463.64f, 14.35f),
			new Vector3(-59.07f, -463.64f, 14.35f)
		};

        /// <summary>
        /// Models used by Gruppe 6 employees.
        /// </summary>
        private string[] securityModels = new string[]
        {
            "M_M_ARMOURED",
			"M_M_SECURITYMAN"
        };

        /// <summary>
        /// Weapons used by Gruppe 6 employees.
        /// </summary>
        private Weapon[] securityWeapons = new Weapon[]
        {
            Weapon.SMG_Uzi,
            Weapon.SMG_MP5,
            Weapon.Handgun_Glock
        };

        // Escorted variables
        private LPed[] pedSecurity = new LPed[4];
        private LVehicle vehSecurity;
        private Blip blipVehicle;
        private bool driverRespectLaws = true;
        private float driverMaxSpeed = CommonCallouts.SPEED_DRIVER_NORMAL;

        // Mission variables
        private int posIndex;
        private bool isStateChanging;

        private ECalloutState savedCalloutState;
        private ECalloutState previousCalloutState;
        private ECalloutState currentCalloutState;

        private EVehicleSecurity currentVehicleSecurity;

        private Blip blipStartPoint;
        
        /// <summary>
        /// 
        /// </summary>
        public override void OnCalloutNotAccepted()
        {
            base.OnCalloutNotAccepted();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool OnBeforeCalloutDisplayed()
        {
            // Arrays with different sizes.
            if (this.startPositions.Length != this.finishPositions.Length)
                return false;

            this.posIndex = Common.GetRandomValue(0, this.startPositions.Length);

            // Check for wrong vectors
            if (this.startPositions[posIndex].Position == Vector3.Zero)
                return false;
            if (this.finishPositions[posIndex] == Vector3.Zero)
                return false;

            // Show info for the player.
            base.AddMinimumDistanceCheck(CommonCallouts.DISTANCE_MEDIUM, this.startPositions[posIndex].Position);
            base.ShowCalloutAreaBlipBeforeAccepting(this.startPositions[posIndex].Position, 10f);
            base.CalloutMessage = string.Format("Requesting units for transport escorting at {0}, please respond.", World.GetStreetName(this.startPositions[posIndex].Position));
            Functions.PlaySoundUsingPosition("THIS_IS_CONTROL INS_AVAILABLE_UNITS_RESPOND_TO IN_OR_ON_POSITION", this.startPositions[posIndex].Position);

            return base.OnBeforeCalloutDisplayed();
        }

        /// <summary>
        /// Called when the callout has been accepted. Call base to set state to Running.
        /// </summary>
        /// <returns>
        /// True if callout was setup properly, false if it failed. Calls <see cref="End"/> when failed.
        /// </returns>
        public override bool OnCalloutAccepted()
        {
            base.OnCalloutAccepted();

            /*
             * Clear area
             */
            GTA.Native.Function.Call("CLEAR_AREA_OF_CARS", new GTA.Native.Parameter[]
			{
				this.startPositions[posIndex].Position.X,
				this.startPositions[posIndex].Position.Y,
				this.startPositions[posIndex].Position.Z,
				5f
			});

            /*
             * Create the security vehicle
             */
            this.vehSecurity = new LVehicle(this.startPositions[posIndex].Position, "STOCKADE");
            if (!CommonCallouts.CheckForObjectExistance(this, this.vehSecurity, false))
            {
                this.End();
                return false;
            }

            Functions.AddToScriptDeletionList(this.vehSecurity, this);

            /*
             * Set enviroment variables for the vehicle.
             */
            this.vehSecurity.Heading = this.startPositions[posIndex].Heading;
            this.vehSecurity.IsRequiredForMission = true;
            //this.vehSecurity.FreezePosition = true;
            this.vehSecurity.MakeProofTo(true, true, false, false, true);

            /*
             * Create the security guards
             */
            for (int i = 0; i < pedSecurity.Length; i++)
            {
                this.pedSecurity[i] = new LPed(this.vehSecurity.Position.Around(3.5f), Common.GetRandomCollectionValue<string>(this.securityModels), LPed.EPedGroup.Cop);
                if (!CommonCallouts.CheckForObjectExistance(this, this.pedSecurity[i], false))
                {
                    this.End();
                    return false;
                }

                Functions.SetPedIsOwnedByScript(this.pedSecurity[i], this, true);
                Functions.AddToScriptDeletionList(this.pedSecurity[i], this);

                /*
                 * Set enviromental variables for guards.
                 */
                this.pedSecurity[i].BecomeMissionCharacter();
                this.pedSecurity[i].IsRequiredForMission = true;
                this.pedSecurity[i].BlockPermanentEvents = true;
                this.pedSecurity[i].Task.AlwaysKeepTask = true;
                this.pedSecurity[i].Task.Wait(-1);
                this.pedSecurity[i].Health = 200;
                this.pedSecurity[i].Armor = 200;
                this.pedSecurity[i].Money = 10000;
                this.pedSecurity[i].RandomizeOutfit();
                this.pedSecurity[i].CanBeArrestedByPlayer = false;
                this.pedSecurity[i].WillDoDrivebys = true;

                /*
                 * Give Weapons
                 */
                this.pedSecurity[i].DefaultWeapon = Common.GetRandomCollectionValue<Weapon>(this.securityWeapons);
                this.pedSecurity[i].EquipWeapon();

                
            }

            /*
             * Set default mission values
             */
            this.currentCalloutState = ECalloutState.STATE_INIT;
            this.previousCalloutState = ECalloutState.STATE_INIT;
            this.savedCalloutState = ECalloutState.STATE_INIT;
            this.currentVehicleSecurity = EVehicleSecurity.SECURITY_NONE;

            this.blipStartPoint = Functions.CreateBlipForArea(this.startPositions[posIndex].Position, 20f);
            this.blipStartPoint.Display = BlipDisplay.MapOnly;
            this.blipStartPoint.RouteActive = true;

            Functions.AddTextToTextwall("Get to the area and escort the security car.", "CONTROL");

            return true;
        }

        /// <summary>
        /// Called every tick to process all script logic. Call base when overriding.
        /// </summary>
        public override void Process()
        {
            base.Process();

            /*
             * Manual ending
             */
            if (this.PlayerEndCallout())
                return;

            /*
             * You died after started the mission.
             */
            if (!LPlayer.LocalPlayer.Ped.IsAlive && this.currentCalloutState != ECalloutState.STATE_INIT)
            {
                base.SetCalloutFinished(false, false, true);
                this.End();
                return;
            }

            /*
             * Vehicle memory check
             */
            if (!CommonCallouts.CheckForObjectExistance(this, this.vehSecurity, true))
                return;

            /*
             * The vehicle is broken.
             */
            if (!this.vehSecurity.IsDriveable || this.vehSecurity.EngineHealth < 150 || this.vehSecurity.Health < 150)
            {
                Functions.AddTextToTextwall("All units, the vehicle is broken, aborting mission", "CONTROL");
                base.SetCalloutFinished(false, false, true);
                this.End();
                return;
            }

            /*
             * Vehicle driver memory
             */
            if (!CommonCallouts.CheckForObjectExistance(this, this.pedSecurity[0], true))
                return;

            /*
             * Driver is dead. The end.
             */
            if(this.pedSecurity[0] != null && !this.pedSecurity[0].IsAliveAndWell)
            {
                Functions.AddTextToTextwall("All units, the vehicle driver is dead.", "CONTROL");
                base.SetCalloutFinished(false, false, true);
                this.End();
                return;
            }

            /*
             * The current state is changed from the previous one.
             */
            this.isStateChanging = (this.previousCalloutState != this.currentCalloutState);

            if (this.isStateChanging)
                CommonCallouts.Log(CommonCallouts.ELogType.LOG_DEBUG, "State changed from [" + this.previousCalloutState + "] to [" + this.currentCalloutState + "]", this);

            /*
             * Check if the player is on the minimum range.
             */
            if(LPlayer.LocalPlayer.Ped.Position.DistanceTo2D(this.vehSecurity.Position) < CommonCallouts.DISTANCE_LONGER)
            {
                this.currentVehicleSecurity = EVehicleSecurity.SECURITY_ESCORTED;

                switch(this.currentCalloutState)
                {
                    /*
                     * Init State. Will be called when player arrives at the security car for the first time.
                     */
                    case ECalloutState.STATE_INIT:
                        if (LPlayer.LocalPlayer.Ped.Position.DistanceTo2D(this.vehSecurity.Position) > CommonCallouts.DISTANCE_CLOSER)
                            break;

                        if (this.blipStartPoint != null && this.blipStartPoint.Exists())
                            this.blipStartPoint.Delete();

                        Functions.AddTextToTextwall("Follow the security car until it reaches the destination.", "CONTROL");
                        this.currentCalloutState = ECalloutState.STATE_WAITING_DRIVER;
                    break;

                    /*
                     * Wait for the security guards enter in the car...
                     */
                    case ECalloutState.STATE_WAITING_DRIVER:
                    {
                        // Check and wait for all passangers
                        if (!CommonCallouts.WaitForPedEnterInVehicle(this, this.pedSecurity, this.vehSecurity, 1, true))
                            break;

                        this.blipVehicle = this.vehSecurity.AttachBlip();
                        this.blipVehicle.Color = BlipColor.Cyan;
                        this.blipVehicle.Display = BlipDisplay.ArrowAndMap;
                        this.blipVehicle.Friendly = true;
                        CommonCallouts.driveVehicleTo(this, this.vehSecurity, this.finishPositions[this.posIndex], this.driverMaxSpeed, this.driverRespectLaws);

                        if (LPlayer.LocalPlayer.Ped.Position.DistanceTo2D(this.vehSecurity.Position) < CommonCallouts.DISTANCE_MEDIUM)
                        {
                            Functions.AddTextToTextwall("Proceeding with the route, please ensure all safety around the objective.", "CONTROL");
                            this.currentCalloutState = ECalloutState.STATE_IN_TRANSIT;
                        }
                    }
                    break;

                    /*
                     * State where the player came back from a previous long distance.
                     */
                    case ECalloutState.STATE_WAITING_PLAYER:
                    {
                        if (LPlayer.LocalPlayer.Ped.Position.DistanceTo2D(this.vehSecurity.Position) > CommonCallouts.DISTANCE_MEDIUM)
                            break;

                        // Check and wait for all passangers
                        if (!CommonCallouts.WaitForPedEnterInVehicle(this, this.pedSecurity, this.vehSecurity, 1, true))
                            break;

                        this.currentCalloutState = this.savedCalloutState;
                        //this.vehSecurity.FreezePosition = false;
                    }
                    break;

                    /* 
                     * State where the security car is going to the destination.
                     */
                    case ECalloutState.STATE_IN_TRANSIT:
                    {
                        if (CommonCallouts.WaitForPedEnterInVehicle(this, this.pedSecurity, this.vehSecurity, 1, true))
                        {
                            CommonCallouts.driveVehicleTo(this, this.vehSecurity, this.finishPositions[this.posIndex], this.driverMaxSpeed, this.driverRespectLaws);

                            // Destination
                            if (this.finishPositions[posIndex].DistanceTo2D(this.vehSecurity.Position) < 2f)
                                this.currentCalloutState = ECalloutState.STATE_ARRIVED;
                        }
                        else
                        {
                            this.vehSecurity.Speed = 0f;
                            this.vehSecurity.EngineRunning = false;
                            this.vehSecurity.HazardLightsOn = true;
                        }
                    }
                    break;

                    /*
                     * Security car arrived at destination.
                     */
                    case ECalloutState.STATE_ARRIVED:
                    {
                        this.vehSecurity.Speed = 0f;
                        this.vehSecurity.EveryoneLeaveVehicle();
                        this.vehSecurity.EngineRunning = false;

                        if (this.isStateChanging)
                            Functions.AddTextToTextwall("All units, the security car is safe now, return to patrol.", "CONTROL");

                        base.SetCalloutFinished(true, true, true);
                        this.End();
                    }
                    break;

                    /*
                     * Should never come here...
                     */
                    default:
                    {
                        CommonCallouts.Log(CommonCallouts.ELogType.LOG_CRITICAL, "Default case of state machine archieved. Never should happen.", this);
                    }
                    break;
                }
            }
            else
            {
                /*
                 * The player went away from the security car...
                 */
                this.currentVehicleSecurity = EVehicleSecurity.SECURITY_NONE;
                switch (this.currentCalloutState)
                {
                    case ECalloutState.STATE_INIT:
                    case ECalloutState.STATE_WAITING_DRIVER:
                    case ECalloutState.STATE_WAITING_PLAYER:
                    case ECalloutState.STATE_ARRIVED:
                    {
                        //TODO
                    }
                    break;

                    default:
                    {
                        if (this.isStateChanging)
                        {
                            Functions.AddTextToTextwall("Attention unit! Return to your position and secure the area.", "CONTROL");
                            this.savedCalloutState = this.currentCalloutState;
                            this.currentCalloutState = ECalloutState.STATE_WAITING_PLAYER;

                            this.vehSecurity.Speed = 0f;
                            this.vehSecurity.HazardLightsOn = true;
                            this.vehSecurity.SoundHorn(1000);
                        }
                    }
                    break;
                }
            }

            /*
             * Save Previous state
             */
            this.previousCalloutState = this.currentCalloutState;
        }

        /// <summary>
        /// Check if player pressed END to finish the callout early.
        /// </summary>
        /// <returns></returns>
        private bool PlayerEndCallout()
        {
            if (Functions.IsKeyDown(Keys.End))
            {
                LPlayer.LocalPlayer.Ped.PlayWalkieTalkieAnimation("RANDOMCHAT_01");
                base.SetCalloutFinished(false, false, false);
                this.End();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Put all resource free logic here. This is either called by the calloutmanager to shutdown the callout or can be called by the 
        /// callout itself to execute the cleanup code. Call base to set state to None.
        /// </summary>
        public override void End()
        {
            base.End();

            if (this.blipStartPoint != null && this.blipStartPoint.Exists())
                this.blipStartPoint.Delete();

            if (this.blipVehicle != null && this.blipVehicle.Exists())
                this.blipVehicle.Delete();

            foreach(LPed ped in this.pedSecurity)
            {
                if(ped != null && ped.Exists() && Functions.DoesPedHaveAnOwner(ped))
                {
                    ped.IsRequiredForMission = false;
                    Functions.RemoveFromDeletionList(ped, this);
                    Functions.SetPedIsOwnedByScript(ped, this, false);
                    ped.NoLongerNeeded();
                }
            }
        }

        /// <summary>
        /// Called when a ped assigned to the current script has left the script due to a more important action, such as being arrested by the player.
        /// This is invoked right before control is granted to the new script, so perform all necessary freeing actions right here.
        /// </summary>
        /// <param name="ped">The ped</param>
        public override void PedLeftScript(LPed ped)
        {
            base.PedLeftScript(ped);
            Functions.RemoveFromDeletionList(ped, this);
            Functions.SetPedIsOwnedByScript(ped, this, false);
        }
    }
}