﻿using GTA;
using LCPD_First_Response.Engine;
using LCPD_First_Response.Engine.Timers;
using LCPD_First_Response.LCPDFR.API;
using LCPD_First_Response.LCPDFR.Callouts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaticalEnforcer.Callouts
{
    class CommonCallouts
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // ADD GENERIC FUNCTIONS BELOW - TRY TO MAKE THEM NOT TARGETED JUST FOR THIS SCRIPT, BUT FOR GENERAL ACTIONS.
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // GENERIC CONSTANTS
        internal const float DISTANCE_CLOSER = 20f;
        internal const float DISTANCE_MEDIUM = 60f;
        internal const float DISTANCE_LONGER = 120f;
        internal const float DISTANCE_VIEWOUT = 180f;

        internal const float SPEED_DRIVER_SLOW = 12f;
        internal const float SPEED_DRIVER_NORMAL = 20f;
        internal const float SPEED_DRIVER_RUSH = 50f;
        internal const float SPEED_DRIVER_RACER = 85f;

        // Minimum level to show debug strings.
        internal const ELogType DEBUG_LEVEL = ELogType.LOG_ERROR;

        /// <summary>
        /// Represent internal debug levels.
        /// </summary>
        internal enum ELogType
        {
            LOG_NONE,
            LOG_INFO,
            LOG_WARNING,
            LOG_ERROR,
            LOG_CRITICAL,
            LOG_DEBUG
        };

        /// <summary>
        /// Enumarate the main regions of GTAIV.
        /// </summary>
        internal enum EPlayerArea
        {
            REGION_ANY,
            REGION_ALGONQUIN,
            REGION_BROKER,
            REGION_DUKES,
            REGION_BOHAN
        }

        /**
         * Check Peds on car. Consider dead peds as 'on car' to avoid confusion.
         */
        internal static bool WaitForPedEnterInVehicle(Callout c, LPed[] pedArray, LVehicle veh, int minPeds = 1, bool waitAll = true)
        {
            int allPeds = 0;
            int numPedInCar = 0;
            int i = 0;

            foreach (LPed ped in pedArray)
            {
                if (!checkForPedCondition(c, ped, true, true, true, false, false))
                    continue;

                allPeds++;
                ped.FreezePosition = false;

                if (!ped.IsInVehicle(veh) && !ped.IsGettingIntoAVehicle)
                {
                    ped.Task.EnterVehicle(veh, i == 0 ? VehicleSeat.Driver : VehicleSeat.AnyPassengerSeat);
                    i++;
                    continue;
                }

                numPedInCar++;
                i++;
            }

            if (waitAll)
                return (numPedInCar == allPeds);
            else
                return (numPedInCar >= minPeds);
        }

        /// <summary>
        /// Check object for existance on both memory and game management.
        /// </summary>
        /// <param name="obj">object to be checked</param>
        /// <returns></returns>
        internal static bool CheckForObjectExistance(Callout c, object obj, bool finishOnError = false)
        {
            bool error = false;

            if (obj == null)
            {
                Log(ELogType.LOG_ERROR, "[Security Check] Object do not exist.", c);
                error = true;
            }
            else
            {
                if (obj.GetType() == typeof(LVehicle) && !((LVehicle)obj).Exists())
                {
                    Log(ELogType.LOG_ERROR, "[Security Check] Vehicle is not null on memory, but do not exists on GTAIV.", c);
                    error = true;
                }

                if (obj.GetType() == typeof(LPed) && !((LPed)obj).Exists())
                {
                    Log(ELogType.LOG_ERROR, "[Security Check] Ped is not null on memory, but do not exists in GTAIV.", c);
                    error = true;
                }
            }

            if (error && finishOnError)
                FinishOnError(c);

            return !error;
        }


        /// <summary>
        /// Check for object existance on array
        /// </summary>
        /// <param name="objArray">object array</param>
        /// <param name="finishOnError">finish callout if any error is found</param>
        /// <returns></returns>
        internal static bool CheckForArrayObjectExistance(Callout c, object[] objArray, bool finishOnError = false)
        {
            bool error = false;
            int numerr = 0;

            if (objArray == null)
            {
                Log(ELogType.LOG_ERROR, "[Security Check] ped array is null.", c);
                return false;
            }

            if (objArray.GetType() == typeof(LPed[]))
            {
                foreach (LPed obj in objArray)
                {
                    if (!CheckForObjectExistance(c, obj, false))
                    {
                        numerr++;
                        error = true;
                    }
                }
            }
            else if (objArray.GetType() == typeof(LVehicle[]))
            {
                foreach (LVehicle obj in objArray)
                {
                    if (!CheckForObjectExistance(c, obj, false))
                    {
                        numerr++;
                        error = true;
                    }
                }
            }
            else
            {
                Log(ELogType.LOG_ERROR, "Unknown object array passed...", c);
            }

            if (error && finishOnError)
                FinishOnError(c);

            return !error;
        }

        /// <summary>
        /// Check for object existance on list
        /// </summary>
        /// <param name="c">Callout</param>
        /// <param name="objList">Object list</param>
        /// <param name="finishOnError">Should finish on error?</param>
        /// <returns></returns>
        internal static bool CheckObjectList(Callout c, List<LPed> objList, bool finishOnError = false)
        {
            bool error = false;
            int numerr = 0;

            if (objList == null)
            {
                Log(ELogType.LOG_ERROR, "[Security Check] ped array is null.", c);
                error = true;
            }
            else
            {
                foreach (LPed obj in objList)
                {
                    if (!CheckForObjectExistance(c, obj, false))
                    {
                        numerr++;
                        error = true;
                    }
                }
            }

            if (error)
            {
                Log(ELogType.LOG_ERROR, "[Security Check] " + numerr + " objects do not exist anymore in GTAIV.", c);
                if (finishOnError)
                    FinishOnError(c);
            }

            return !error;
        }

        /// <summary>
        /// Check for object existance on list
        /// </summary>
        /// <param name="c">Callout</param>
        /// <param name="objList">Object list</param>
        /// <param name="finishOnError">Should finish on error?</param>
        /// <returns></returns>
        internal static bool CheckObjectList(Callout c, List<LVehicle> objList, bool finishOnError = false)
        {
            bool error = false;
            int numerr = 0;

            if (objList == null)
            {
                Log(ELogType.LOG_ERROR, "[Security Check] ped array is null.", c);
                error = true;
            }
            else
            {
                foreach (LVehicle obj in objList)
                {
                    if (!CheckForObjectExistance(c, obj, false))
                    {
                        numerr++;
                        error = true;
                    }
                }
            }

            if (error)
            {
                Log(ELogType.LOG_ERROR, "[Security Check] " + numerr + " objects do not exist anymore in GTAIV.", c);
                if (finishOnError)
                    FinishOnError(c);
            }

            return !error;
        }

        /// <summary>
        /// Check for common ped attributes
        /// </summary>
        /// <param name="isDead">check if the ped is dead</param>
        /// <param name="isArrasted">check if the ped is arrasted</param>
        /// <returns></returns>
        internal static int CheckForPedBadArray(Callout c, LPed[] pedArray, bool isDead, bool isArrasted)
        {
            return CheckForPedArray(c, pedArray, false, false, false, isDead, isArrasted);
        }

        /// <summary>
        /// Check for common ped attributes
        /// </summary>
        /// <param name="pedArray">ped array to be checked</param>
        /// <param name="isAlive">check for alive</param>
        /// <param name="isFree">check if the ped is free</param>
        /// <param name="isWell">check if the ped is well</param>
        /// <returns></returns>
        internal static int CheckForPedArray(Callout c, LPed[] pedArray, bool isAlive, bool isFree, bool isWell)
        {
            return CheckForPedArray(c, pedArray, isAlive, isFree, isWell, false, false);
        }

        /// <summary>
        /// Check for common ped attributes
        /// </summary>
        /// <param name="pedArray">ped array to be checked</param>
        /// <param name="isAlive">check for alive</param>
        /// <param name="isFree">check if the ped is free</param>
        /// <param name="isWell">check if the ped is well</param>
        /// <param name="isDead">check if the ped is dead</param>
        /// <param name="isArrasted">check if the ped is arrasted</param>
        /// <returns></returns>
        internal static int CheckForPedArray(Callout c, LPed[] pedArray, bool isAlive, bool isFree, bool isWell, bool isDead, bool isArrasted)
        {
            int result = 0;
            for (int i = 0; i < pedArray.Length; i++)
            {
                if (!checkForPedCondition(c, pedArray[i], isAlive, isFree, isWell, isDead, isArrasted))
                    continue;

                result++;
            }

            return result;
        }

        /// <summary>
        /// Check for common parameters in ped
        /// </summary>
        /// <param name="ped"></param>
        /// <param name="isAlive"></param>
        /// <param name="isFree"></param>
        /// <param name="isWell"></param>
        /// <param name="isDead"></param>
        /// <param name="isArrasted"></param>
        /// <returns></returns>
        internal static bool checkForPedCondition(Callout c, LPed ped, bool isAlive, bool isFree, bool isWell, bool isDead, bool isArrasted)
        {
            if (!CheckForObjectExistance(c, ped))
                return false;

            if (isWell && !ped.IsAliveAndWell)
                return false;

            if (isAlive && !ped.IsAlive)
                return false;

            /*if (isFree && !ped.IsBeingArrested)
                return false;

            if (isFree && ped.HasBeenArrested)
                return false;*/

            if (isDead && !ped.IsDead)
                return false;

            if (isArrasted && !ped.HasBeenArrested)
                return false;

            return true;
        }

        /// <summary>
        /// Takes a vehicle and make this go to a destination.
        /// </summary>
        /// <param name="c">Callout</param>
        /// <param name="veh">Vehicle</param>
        /// <param name="destPos">Route vector array</param>
        /// <param name="speed">Maximum speed</param>
        /// <param name="followLaws">Follow traffic laws</param>
        /// <param name="useWrongWay">Can use wrong ways to drive</param>
        /// <returns>true when successful, false otherwise</returns>
        internal static bool driveVehicleTo(Callout c, LVehicle veh, Vector3 destPos, float speed, bool followLaws = true, bool useWrongWay = false)
        {
            if (!CommonCallouts.CheckForObjectExistance(c, veh))
                return false;

            veh.FreezePosition = false;
            veh.HazardLightsOn = false;

            LPed pedDriver = veh.GetPedOnSeat(VehicleSeat.Driver);
            if (!CommonCallouts.CheckForObjectExistance(c, pedDriver))
                return false;

            pedDriver.Task.DriveTo(destPos, speed, followLaws, useWrongWay);
            return true;
        }

        /// <summary>
        /// Takes a vehicle and make this go to a destination using routes.
        /// </summary>
        /// <param name="c">Callout</param>
        /// <param name="veh">Vehicle</param>
        /// <param name="route">Route vector array</param>
        /// <param name="speed">Maximum speed</param>
        /// <param name="followLaws">Follow traffic laws</param>
        /// <returns>true when successful, false otherwise</returns>
        internal static bool driveVehicleTo(Callout c, LVehicle veh, Vector3[] route, float speed, bool followLaws = true)
        {
            if (!CommonCallouts.CheckForObjectExistance(c, veh))
                return false;

            veh.FreezePosition = false;
            veh.HazardLightsOn = false;

            LPed pedDriver = veh.GetPedOnSeat(VehicleSeat.Driver);
            if (!CommonCallouts.CheckForObjectExistance(c, pedDriver))
                return false;

            pedDriver.Task.ClearAllImmediately();
            pedDriver.Task.DrivePointRoute(veh, speed, route);
            return true;
        }

        /// <summary>
        /// Finish the callout showing an error message.
        /// </summary>
        internal static void FinishOnError(Callout c)
        {
            Log(ELogType.LOG_CRITICAL, "Resource doesn't exist. Finishing callout.", c);
            Functions.AddTextToTextwall("TaticalEnforcer: an exception was handled properly; a resource doesn't exist.", "SYSTEM");
            Functions.StopCurrentCallout();
            c.End();
        }

        internal static void Log(ELogType type, string message, Callout c)
        {
            switch(type)
            {
                case ELogType.LOG_INFO:
                    LCPD_First_Response.Engine.Log.Info(message, c);
                break;

                case ELogType.LOG_WARNING:
                    LCPD_First_Response.Engine.Log.Warning(message, c);
                break;

                case ELogType.LOG_ERROR:
                    LCPD_First_Response.Engine.Log.Error(message, c);
                break;

                case ELogType.LOG_CRITICAL:
                    LCPD_First_Response.Engine.Log.Error(message, c);
                break;

                case ELogType.LOG_DEBUG:
                    LCPD_First_Response.Engine.Log.Error("[DEBUG] " + message, c);
                break;
            }

            if(type >= DEBUG_LEVEL)
            {
                int i = 0;
                List<string> logMessages = new List<string>();
                foreach (StackFrame sf in new StackTrace().GetFrames())
                {
                    i++;
                    LCPD_First_Response.Engine.Log.Error(sf.ToString(), c, true);
                    if (i == 4)
                        break;
                }
            }
        }
    }
}
