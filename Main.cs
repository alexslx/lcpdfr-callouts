﻿using System.Windows.Forms;

using TaticalEnforcer.Callouts;

using GTA;

using LCPD_First_Response.Engine;
using LCPD_First_Response.Engine.Input;
using LCPD_First_Response.Engine.Scripting.Plugins;
using LCPD_First_Response.LCPDFR.API;

using SlimDX.XInput;

namespace TaticalEnforcer
{
    /// <summary>
    /// Tatical Enforcer Plugin
    /// </summary>
    [PluginInfo("TaticalEnforcer", false, true)]
    public class TaticalEnforcer : Plugin
    {
        /// <summary>
        /// Called when the plugin has been created successfully.
        /// </summary>
        public override void Initialize()
        {
            // Bind console commands
            this.RegisterConsoleCommands();

            // Listen for on duty event
            Functions.OnOnDutyStateChanged += this.Functions_OnOnDutyStateChanged;

            Log.Info("Started", this);
        }

        /// <summary>
        /// Called when player changed the on duty state.
        /// </summary>
        /// <param name="onDuty">The new on duty state.</param>
        public void Functions_OnOnDutyStateChanged(bool onDuty)
        {
            if (onDuty)
            {
                // Register callouts to LCPDFR
                Functions.RegisterCallout(typeof(BankTransport));
            }
        }

        /// <summary>
        /// Called every tick to process all plugin logic.
        /// </summary>
        public override void Process()
        {
            
        }

        /// <summary>
        /// Called when the plugin is being disposed, e.g. because an unhandled exception occured in Process. Free all resources here!
        /// </summary>
        public override void Finally()
        {

        }
    }
}